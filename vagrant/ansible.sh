apt-get update
apt-get install -y git
apt-get install -y python-software-properties
apt-add-repository -y ppa:ansible/ansible
apt-get install -y ansible
co /home/vagrant
git clone https://hipbone@bitbucket.org/hipbone/dev-ubuntu14-basic.git
chown -R vagrant:vagrant dev-ubuntu14-basic
cd dev-ubuntu14-basic
ansible-playbook -i hosts site.yml
